-- | Unicode support
--
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric  #-}
module Xeno.Unicode
    ( ByteStringUTF16LE(..)
    , ByteStringUTF16BE(..)
    , ByteStringZeroTerminatedUTF16LE(..)
    , ByteStringZeroTerminatedUTF16BE(..)
    , WithByteOrder(..)
    , interpretAsUTF16
    , convertToZeroTerminated

    )
    where

import           Control.DeepSeq
import           Data.ByteString (ByteString)
import qualified Data.ByteString as S
import qualified Data.ByteString.Unsafe as SU
import           Data.Word
import           GHC.Generics
import           Numeric(showHex)

import Xeno.Types


-- Note: UTF-16 have two representation: little endian and big endian.
--       We want to support fast processing of strings, so we omit not use
--       `if littleEndian then ... else ...`, nor other tricks with implicit `if`s
--
--       Instead we propose instance for both endians; special selector `DataWithByteOrder`
--       and function `interpretAsUTF16` which recognize BOM and return appropriate instance.


-- | ByteString with UTF-16
newtype ByteStringUTF16LE = BSUTF16LE ByteString deriving (Generic, NFData)

newtype ByteStringUTF16BE = BSUTF16BE ByteString deriving (Generic, NFData)

-- | ByteString whith UTF-16 wich guaranted have '\NUL' at the end
newtype ByteStringZeroTerminatedUTF16LE = BSZTUTF16LE ByteString deriving (Generic, NFData)

newtype ByteStringZeroTerminatedUTF16BE = BSZTUTF16BE ByteString deriving (Generic, NFData)


instance VectorizedString ByteStringUTF16LE where
    s_index' (BSUTF16LE ps) n = ps `S.index` (n * 2)
    {-# INLINE s_index' #-}
    elemIndexFrom' w (BSUTF16LE bs) i = (`div` 2) <$> (elemIndexFrom w bs (i * 2))
    {-# INLINE elemIndexFrom' #-}
    drop' i (BSUTF16LE bs) = BSUTF16LE $ S.drop (i * 2) bs
    {-# INLINE drop' #-}
    substring' (BSUTF16LE bs) s t = substring bs (s * 2) (t * 2)
    {-# INLINE substring' #-}
    toBS (BSUTF16LE bs) = bs
    {-# INLINE toBS #-}


instance VectorizedString ByteStringUTF16BE where
    s_index' (BSUTF16BE ps) n = ps `S.index` (n * 2 + 1)
    {-# INLINE s_index' #-}
    elemIndexFrom' w (BSUTF16BE bs) i = (`div` 2) <$> (elemIndexFrom w bs (i * 2))
    {-# INLINE elemIndexFrom' #-}
    drop' i (BSUTF16BE bs) = BSUTF16BE $ S.drop (i * 2) bs
    {-# INLINE drop' #-}
    substring' (BSUTF16BE bs) s t = substring bs (s * 2) (t * 2)
    {-# INLINE substring' #-}
    toBS (BSUTF16BE bs) = bs
    {-# INLINE toBS #-}


instance VectorizedString ByteStringZeroTerminatedUTF16LE where
    s_index' (BSZTUTF16LE ps) n = ps `SU.unsafeIndex` (n * 2)
    {-# INLINE s_index' #-}
    elemIndexFrom' w (BSZTUTF16LE bs) i = (`div` 2) <$> (elemIndexFrom w bs (i * 2))
    {-# INLINE elemIndexFrom' #-}
    drop' i (BSZTUTF16LE bs) = BSZTUTF16LE $ S.drop (i * 2) bs
    {-# INLINE drop' #-}
    substring' (BSZTUTF16LE bs) s t = substring bs (s * 2) (t * 2)
    {-# INLINE substring' #-}
    toBS (BSZTUTF16LE bs) = bs
    {-# INLINE toBS #-}


instance VectorizedString ByteStringZeroTerminatedUTF16BE where
    s_index' (BSZTUTF16BE ps) n = ps `SU.unsafeIndex` (n * 2 + 1)
    {-# INLINE s_index' #-}
    elemIndexFrom' w (BSZTUTF16BE bs) i = (`div` 2) <$> (elemIndexFrom w bs (i * 2))
    {-# INLINE elemIndexFrom' #-}
    drop' i (BSZTUTF16BE bs) = BSZTUTF16BE $ S.drop (i * 2) bs
    {-# INLINE drop' #-}
    substring' (BSZTUTF16BE bs) s t = substring bs (s * 2) (t * 2)
    {-# INLINE substring' #-}
    toBS (BSZTUTF16BE bs) = bs
    {-# INLINE toBS #-}


data WithByteOrder a b = LittleEndian a
                       | BigEndian    b


-- | Recognize BOM and returns appropriate data type
interpretAsUTF16 :: ByteString -> WithByteOrder ByteStringUTF16LE ByteStringUTF16BE
interpretAsUTF16 bs
    | S.length bs < 2          = error "String too short to recognize BOM"
    | b1 == 0xFF && b2 == 0xFE = LittleEndian $ BSUTF16LE (SU.unsafeDrop 2 bs)
    | b1 == 0xFE && b2 == 0xFF = BigEndian    $ BSUTF16BE (SU.unsafeDrop 2 bs)
    | otherwise                = error $ "Unrecognized BOM: " ++ bomStr
  where
    b1 = bs `SU.unsafeIndex` 0
    b2 = bs `SU.unsafeIndex` 1
    bomStr = concat ["0x", showHex b1 "", " 0x", showHex b2 ""]


-- | Convert string to zero-terminated string.
convertToZeroTerminated :: WithByteOrder ByteStringUTF16LE ByteStringUTF16BE
                        -> WithByteOrder ByteStringZeroTerminatedUTF16LE ByteStringZeroTerminatedUTF16BE
convertToZeroTerminated (LittleEndian (BSUTF16LE bs)) = LittleEndian (BSZTUTF16LE bs)
convertToZeroTerminated (BigEndian    (BSUTF16BE bs)) = BigEndian    (BSZTUTF16BE bs)


-- | Get index of an element starting from offset.
elemIndexFrom :: Word8 -> ByteString -> Int -> Maybe Int
elemIndexFrom c str offset = fmap (+ offset) (S.elemIndex c (S.drop offset str))


-- | Get a substring of a string.
substring :: ByteString -> Int -> Int -> ByteString
substring s start end = S.take (end - start) (S.drop start s)

